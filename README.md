# HHR

Пакет `HHR` представляет собой R-клиент для работы с API сайта [hh.ru](https://api.hh.ru/). На данный момент пакет находится на стадии прототипа и не предназначен для использования обычными пользователями.

## Установка

Чтобы установить последнюю версию пакета из гит-репозитория необходимо выполнить следующую команду:

```r
devtools::install_bitbucket("unikum/hhr")
```

## Использование

```r
library(HHR)
library(httr)
library(jsonlite)
api_url <- "https://api.hh.ru/"
hh_token <- authorize("Client ID", "Client secret")
data_resp <- GET(api_url, path = "me", config = config(token = hh_token))
data_json <- fromJSON(content(data_resp, as = "text"))
```

## Отчёты об ошибках

При возникновении ошибок в работе пакета для получения дополнительной информации выполните команды, задействующие функции из пакета `HHR`, с помощью функции `httr::with_verbose()`.

Чтобы получить необходимую информацию об установленном ПО и версиях пакетов, а также перейти на страницу для создания отчёта об ошибке, напечатайте в консоли R следующую команду:

```r
utils::bug.report(package = "HHR")
```
